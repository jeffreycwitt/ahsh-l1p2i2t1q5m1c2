<?xml version="1.0" encoding="UTF-8"?><?xml-model href="https://raw.githubusercontent.com/lombardpress/lombardpress-schema/master/src/out/critical.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?><?xml-model href="https://raw.githubusercontent.com/lombardpress/lombardpress-schema/master/src/out/critical.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?><TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>I, P. 2, Inq. 2, Tract. 1, Q. 5, M. 1, C. 2</title>
        <author ref="#AlexanderOfHales">Alexander of Hales</author>
        <respStmt>
          <name xml:id="JW">Jeffrey C. Witt</name>
          <resp>TEI encoder</resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition n="0.0.0-dev">
          <date when="2017-04-21">April 21, 2017</date>
        </edition>
      </editionStmt>
      <publicationStmt>
        <authority>
          <ref target="http://lydiaschumacher.wixsite.com/earlyfranciscans">ERC Project 714427: Authority and Innovation in Early Franciscan Thought</ref>
        </authority>
        <availability status="free">
          <p>Published under a
            <ref target="http://creativecommons.org/licenses/by-nc-nd/3.0/">Creative Commons Attribution-NonCommercial-NoDerivs 3.0 License</ref>
          </p>
        </availability>
      </publicationStmt>
      <sourceDesc>
        <listWit>
          <witness xml:id="Q" n="quaracchi1924">Quaracchi 1924</witness>
          <witness xml:id="V" n="vatlat701">Vat lat 701</witness>
          <witness xml:id="B" n="borghlat359">Borgh. lat 359</witness>
          <witness xml:id="U" n="urblat123">Urb lat 123</witness>
          <witness xml:id="Pa" n="bnf15331">BnF 15331</witness>
        </listWit>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
     <schemaRef n="lbp-critical-1.0.0" url="https://raw.githubusercontent.com/lombardpress/lombardpress-schema/master/src/out/critical.rng"/>
     <editorialDecl>
       <p>Encoding of this text has followed the recommendations of the LombardPress 1.0.0
         guidelines for a critical edition.</p>
     </editorialDecl>
   </encodingDesc>
   <revisionDesc status="draft">
     <listChange>
       <change when="2017-04-21" status="draft" n="0.0.0">
         <p>Created file for the first time.</p>
       </change>
     </listChange>
   </revisionDesc>
  </teiHeader>
  <text xml:lang="la">
    <front>
      
      <div xml:id="starts-on">
        <pb ed="#Q" n="551"/><cb ed="#Q" n="a"/>
      </div>
    </front>
    <body>
      <div xml:id="ahsh-l1p2i2t1q5m1c2">
        <head xml:id="ahsh-l1p2i2t1q5m1c2-Hd1e118">I, P. 2, Inq. 2, Tract. 1, Q. 5, M. 1, C. 2</head>
        <head xml:id="ahsh-l1p2i2t1q5m1c2-Hd1e121" type="question-title">UTRUM DEUS OMNIUM RERUM SIT DOMINUS EX TEMPORE.</head>
        <p xml:id="ahsh-l1p2i2t1q5m1c2-d1e124">
          <lb ed="#Q"/>Secundo quaeritur utrum Dominust omnium re<lb ed="#Q"/>rum
          sit ex tempore.
        </p>
        <p xml:id="ahsh-l1p2i2t1q5m1c2-d1e131">
          <lb ed="#Q"/>Ad quod sic abiciet aliquis 6: l. « Quia non tan<lb ed="#Q"/>tum
          est Dominus rerum quae coeperunt ex tem<lb ed="#Q"/>pore,
          sed etiam illius rei quae non coepit ex
          <lb ed="#Q"/>tempore, id est ipsius temporis, quod non ince<lb ed="#Q"/>pit
          in tempore, quia non erat tempus antequam
          <lb ed="#Q"/>inciperet: et ideo non coepit esse Dominus ex
          <lb ed="#Q"/>tempore ».
        </p>
        <p xml:id="ahsh-l1p2i2t1q5m1c2-d1e149">
          <cb ed="#Q" n="b"/>
          <lb ed="#Q"/>Respondet <ref><name ref="#Lombard">Magister</name>, in Sententiis, I libro,
          <lb ed="#Q"/>dist. 30</ref>, quod 
          <cit>
            <quote xml:id="ahsh-l1p2i2t1q5m1c2-Qd1e164" source="http://scta.info/resource/pll1d30c1-d1e3541">
              «licet non coeperit ex tempore esse
              <lb ed="#Q"/>Dominus temporis, coepit tamen esse Dominus
              <lb ed="#Q"/>temporis, quia non semper fuit tempus, et ipsius
              <lb ed="#Q"/>hominis ex tempore coepit esse Dominus »
            </quote>
            <bibl>
              <ref target="http://scta.info/resource/pll1d30c1-d1e3541">Lombardus, Sent. I, d. 30, c. 1 [n.5]</ref>
            </bibl>
          </cit>. 
          Unde
          <lb ed="#Q"/>A u g u s ti n u 5, V De Trinitate 7: « Quando coe<lb ed="#Q"/>pit
          " tempus, Deus coepit esse Dominus, nec ante
          <lb ed="#Q"/>tempus tuit Dominus, sed cum tempore, non ex
          <lb ed="#Q"/>tempore vel in tempore, quia non ante fuit tempus
          <lb ed="#Q"/>quam ipse Dominus, sed simul ».
        </p>
        <p xml:id="ahsh-l1p2i2t1q5m1c2-d1e193">
          <pb ed="#Q" n="552"/>
          <cb ed="#Q" n="a"/>
          <lb ed="#Q"/>Sed obicitur: a. Quia", secundum ls i d 0 r u m ',
          <lb ed="#Q"/>quatuor sunt coaeva: tempus, caelum empyreum,
          <lb ed="#Q"/>prima materia et angeli; ergo si est Dominus il<lb ed="#Q"/>lorum
          trium ex tempore vel in tempore, erit Do<lb ed="#Q"/>minus
          temporis' ex tempore; sed non est Dominus
          <lb ed="#Q"/>temporis ex tempore; ergo nec aliorum. Si enim
          <lb ed="#Q"/>ratio est" quod non coepit esse : Dominus tem<lb ed="#Q"/>poris
          ex tempore, quia non erat'* tempus ante
          <lb ed="#Q"/>tempus, eadem ratione non fuit Dominus aliorum
          <lb ed="#Q"/>trium ex tempore, quia tempus * non tuit ante illa.
        </p>
        <p xml:id="ahsh-l1p2i2t1q5m1c2-d1e222">
          <lb ed="#Q"/>b. Item, supposito quod omnia sint simul creata,
          <lb ed="#Q"/>secundum opinionem A ugusti ni 2 - secundum
          <lb ed="#Q"/>illud Eccli. 18,l: Qui vivit in aeternum, creavit
          <lb ed="#Q"/>omnia simuI,'et hoc est vel in genere vel in specie
          — igiiur, cum omnis creatura sit coaequaeva tem<lb ed="#Q"/>pori],
          videtur quod non sit Dominus creaturarum
          <lb ed="#Q"/>ex tempore vel in tempore sicut nec Dominus
          <lb ed="#Q"/>temporis.
        </p>
        <p xml:id="ahsh-l1p2i2t1q5m1c2-d1e240">
          <lb ed="#Q"/>c. item, « cum Deus coeperit! esse Dominus
          <lb ed="#Q"/>temporis cum tempore », sicut" dicit Au gusti<lb ed="#Q"/>n
          u s3, quaeritur' utrum similiter possit dici e con<lb ed="#Q"/>verso
          quod tempus coeperit esse cum Domino.
        </p>
        <p xml:id="ahsh-l1p2i2t1q5m1c2-d1e251">
          <lb ed="#Q"/>Respondeo quod Deus coepit esse Dominus
          <lb ed="#Q"/>ex" tempore rerum coaequaevarum tempori se<lb ed="#Q"/>cundum
          quod ' Dominus ' dicitur in actu. Et ratio
          <lb ed="#Q"/>huius est: quia non potest intelligi creatura quin
          <lb ed="#Q"/>intelligatur ens de nihilo, et sic intelligitur mu<lb ed="#Q"/>tabilitas,
          quae non potest intelligi sine tempore;
          <lb ed="#Q"/>unde non potest intelligi creatura quin intelliga<lb ed="#Q"/>tur'
          tempus; propter hoc oportet dicere quod
          <lb ed="#Q"/>coepit esse Dominus temporis cum tempore. Sed
          <cb ed="#Q" n="b"/>
          <lb ed="#Q"/>quia tempus possumus intelligere non intelligendo
          <lb ed="#Q"/>creaturam hanc vel illam, propter hoc possumus
          <lb ed="#Q"/>dicere de aliis creaturis quod coepit esse Dominus
          <lb ed="#Q"/>illarum ex tempore, quia potest intelligi tempus
          <lb ed="#Q"/>ante creaturam. Coepit ergo esse Dominus tem<lb ed="#Q"/>poris
          cum tempore, aliarum rerum ex tempore.
        </p>
        <p xml:id="ahsh-l1p2i2t1q5m1c2-d1e288">
          <lb ed="#Q"/>[Ad obiecta]: 1. Ad hoc quod obicitur di<lb ed="#Q"/>cendum"
          quod 'incipere esse' non convenit Do- '
          <lb ed="#Q"/>mino secundum se, sed per illud" ad quod re<lb ed="#Q"/>fertur,
          cui secundum se convenit incipere esse,
          <lb ed="#Q"/>et ideo proprie dicitur de Domino quod coepit
          <lb ed="#Q"/>esse Dominus cum tempore, sed non e converso,
          <lb ed="#Q"/>quia tunc non " diceretur solum de Domino quod
          <lb ed="#Q"/>coepit esse propter relationem, sed etiam propter
          <lb ed="#Q"/>substantiam. — Aliter potest dici quod haec prae<lb ed="#Q"/>positio
          'cum' dicit associationem sive simulta<lb ed="#Q"/>tem,
          sed quandoque cum paritate, ut cum di<lb ed="#Q"/>citur':
          Tecum principlum; quandoque non cum
          <lb ed="#Q"/>paritate, et hoc dupliciter, quia quandoque no<lb ed="#Q"/>taturf'
          auctoritas in ablativo et subauctorita's in
          <lb ed="#Q"/>nominativo, ut cum dicitur ' miles est cum rege ',
          <lb ed="#Q"/>quandoque" e converso, ut I Cor. 15,1o: Non
          <lb ed="#Q"/>ego, sed gratia Dei mecum. Quando' ergo nota<lb ed="#Q"/>tur
          associatio cum paritate, potest fieri conversio;
          <lb ed="#Q"/>quando vero notatur non cum paritate, non potest
          <lb ed="#Q"/>fieri conversio; unde non' sequitur: 'miies est
          <lb ed="#Q"/>cum rege, ergo rex est cum milite' ; quia ergo,
          <lb ed="#Q"/>cum dicitur ' Deus coepit esse Dominus cum
          <lb ed="#Q"/>tempore ', notatur' associatio non cum paritate,
          <lb ed="#Q"/>non potest fieri conversio ut dicatur 'tempus 
          <lb ed="#Q"/>coepit esse cum Domino's.
        </p>
      </div>
    </body>
  </text>
</TEI>